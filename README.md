# External entities field Processors

This module allows developers to add custom data formatters to fields imported
with external entities module.

## Table of contents

- Requirements
- Installation
- Configuration
- FAQ
- Maintainers

## Requirements

This module requires the following modules:
- [External entities](https://www.drupal.org/project/external_entities)

## Installation (required, unless a separate INSTALL.md is provided)

Install as you would normally install a contributed Drupal module. For further
information, see 
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.
2. If you already have an exernal entitie created, go to its edition page. 
3. If not, create one as you would normally.
3. On Field mapper, select Fields processors mapper
4. Select for each mapped field, the processor you want to use.

## FAQ 
**Q: How do I write a field processor?**

**A:** Create a custom module, and add a new class in 
src/Plugin/ExternalEntities/FieldMapProcessor/. Create a class that extends 
FieldMapProcessorBase and add an annotation like this:
```
/**
 * This is a description of what my processor does.
 *
 * @FieldMapProcessor (
 *   id = "mi_own_processor",
 *   label = @Translation("My Processor"),
 *   description = @Translation("Thiw is what my processor does."), 
 *   field_types = "text,textarea"
 * )
 */
```

## Maintainers
- Juanjo López - [Juanjol](https://www.drupal.org/u/juanjol)
