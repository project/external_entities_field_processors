<?php

namespace Drupal\external_entities_field_processors\FieldMapProcessor;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\external_entities\ExternalEntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for field map processors.
 *
 * @todo Implements ConfigurableInterface, PluginFormInterface, use
 * PluginFormTrait and add the ability to configure processors.
 */
abstract class FieldMapProcessorBase extends PluginBase implements FieldMapProcessorInterface {

  use LoggerChannelTrait;

  /**
   * The external entity type this field mapper is configured for.
   *
   * @var \Drupal\external_entities\ExternalEntityTypeInterface
   */
  protected $externalEntityType;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Extra data to be used by the processor.
   *
   * @var array
   */
  protected $extraData = [];

  /**
   * Constructs a FieldMapperBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The identifier for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    if (!empty($configuration['_external_entity_type']) && $configuration['_external_entity_type'] instanceof ExternalEntityTypeInterface) {
      $this->externalEntityType = $configuration['_external_entity_type'];
      unset($configuration['_external_entity_type']);
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $this->getLogger($this->pluginDefinition['provider']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Gets Processor label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Gets Processor description.
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * Gets fields types that this processor can be applied for.
   */
  public function getAplicableFieldTypes() {
    return $this->pluginDefinition['field_types'] ?? [];
  }

  /**
   * Add extra data to be used by the processor.
   */
  public function setExtraData(array $extra_data) {
    $this->extraData = $extra_data;
  }

  /**
   * Add extra data to be used by the processor.
   */
  public function getExtraData($key = NULL) {
    if ($key) {
      return $this->extraData[$key] ?? NULL;
    }
    return $this->extraData;
  }

  /**
   * Gets the processed data.
   */
  public function processData($raw_data) {
    return $raw_data;
  }

}
