<?php

namespace Drupal\external_entities_field_processors\FieldMapProcessor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Interface for field mapper plugins.
 *
 * Field mappers control how raw data is mapped into and out of entity objects.
 */
interface FieldMapProcessorInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Constructs a new field map processor plugin.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager);

  /**
   * Returns the administrative label for this field map processor plugin.
   *
   * @return string
   *   The field map processor administrative label.
   */
  public function getLabel();

  /**
   * Returns the administrative description for this field processor plugin.
   *
   * @return string
   *   The field map processor administrative description.
   */
  public function getDescription();

  /**
   * Returns the field types this field map processor applies to.
   *
   * @return array
   *   The field map processor applicable field types.
   */
  public function getAplicableFieldTypes();

  /**
   * Returns the processed data.
   *
   * @param string|array $raw_data
   *   The raw data.
   *
   * @return array
   *   The processed data.
   */
  public function processData($raw_data);

}
