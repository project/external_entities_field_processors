<?php

namespace Drupal\external_entities_field_processors\Plugin\ExternalEntities\FieldMapper;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\external_entities\Plugin\ExternalEntities\FieldMapper\SimpleFieldMapper;
use Drupal\external_entities_field_processors\FieldMapProcessor\FieldMapProcessorManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field mapper based on SimpleFieldMapper with processors fields added.
 *
 * @FieldMapper(
 *   id = "field_map_processors_mapper",
 *   label = @Translation("Fields processors mapper"),
 *   description = @Translation("Maps entity field to raw data and allow to
 *     select diferent processors for each field property.")
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\FieldMapper
 */
class FieldMapProcessorsMapper extends SimpleFieldMapper {

  /**
   * The field map processor manager.
   *
   * @var \Drupal\external_entities_field_processors\FieldMapProcessor\FieldMapProcessorManager
   */
  protected FieldMapProcessorManager $fieldMapProcessorManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->fieldMapProcessorManager = $container->get('plugin.manager.external_entities_field_processors.field_map_processor_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);
    $form['field_mappings']['#type'] = 'container';
    return $form;
  }

  /**
   * Build a form element for configuring a field mapping.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   The form element for configuring the field.
   */
  protected function buildFieldMappingElement(FieldDefinitionInterface $field_definition): array {
    $element = [];

    $property_definitions = $this->getMappableFieldProperties($field_definition);
    foreach ($property_definitions as $property_name => $property_definition) {
      $field_name = $field_definition->getName();
      $element[$property_name] = $this->buildFieldPropertyMappingElement($field_definition, $property_name, $property_definition);
      $element['processor_' . $property_name] = $this->buildFieldProcessorSelector($field_name, $property_name, $field_definition->getLabel() . ' » ' . $property_definition->getLabel());
    }

    return $element;
  }

  /**
   * Return the field property processor select element.
   *
   * @param string $field_name
   *   The field name.
   * @param string $property_name
   *   The field property name.
   * @param string $label
   *   The selector label.
   *
   * @return array
   *   The form element for selecting the field property processor.
   */
  protected function buildFieldProcessorSelector(string $field_name, string $property_name, string $label = ''): array {
    $processors = $this->fieldMapProcessorManager->getDefinitions();
    $options = ['' => '- None -'];
    foreach ($processors as $processor_id => $processor) {
      $options[$processor_id] = $processor['label'];
    }
    $element = [
      '#type' => 'select',
      '#title' => $this->t('@property_name Processor', ['@property_name' => $label]),
      '#options' => $options,
      '#default_value' => $this->getFieldPropertyProcessor($field_name, $property_name),

    ];
    return $element;
  }

  /**
   * Return the current field property processor.
   *
   * @param string $field_name
   *   The field name.
   * @param string $property_name
   *   The field property name.
   *
   * @return string
   *   The field property processor id.
   */
  protected function getFieldPropertyProcessor(string $field_name, string $property_name): string {
    $field_mappings = $this->getFieldMappings();

    if (empty($field_mappings[$field_name])) {
      return '';
    }

    if (empty($property_name) && isset($field_mappings['processor_' . $field_name])) {
      return $field_mappings['processor_' . $field_name];
    }

    return $field_mappings[$field_name]['processor_' . $property_name] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  protected function processPropertyValue(FieldDefinitionInterface $field_definition, $property_name, $property_value) {
    if (!$property_value) {
      return NULL;
    }

    $property_definition = $field_definition
      ->getFieldStorageDefinition()
      ->getPropertyDefinition($property_name);
    $typed_data = $this->typedDataManager->create($property_definition);

    $typed_data->setValue($property_value);
    // Convert the property value to the correct PHP type as expected by this
    // specific property type.
    if ($typed_data instanceof PrimitiveInterface) {
      $property_value = $typed_data->getCastedValue();
    }

    return $property_value;
  }

  /**
   * {@inheritdoc}
   */
  protected function isProcessorMapperField($property_name): bool {
    return str_starts_with($property_name, 'processor_');
  }

  /**
   * Extracts field values from raw data for a given field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Definition of the field we are extracting for.
   * @param array $raw_data
   *   The raw data to extract the field values from.
   * @param array &$context
   *   Any contextual data that needs to be maintained during the whole
   *   extraction process for an external entity.
   *
   * @return array
   *   An array of field values, or NULL if none.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function extractFieldValuesFromRawData(FieldDefinitionInterface $field_definition, array $raw_data, array &$context) {
    $property_mappings = $this->getFieldMapping($field_definition->getName());
    if (empty($property_mappings)) {
      return NULL;
    }

    $properties_values = [];
    foreach ($property_mappings as $property_name => $mapping) {
      if ($this->isConstantValueMapping($property_name) || $this->isProcessorMapperField($property_name)) {
        continue;
      }
      $extra_data = [];
      if ($this->haveExtraMappingData($mapping)) {
        $extra_data = $this->getExtraMappingData($mapping);
        $mapping = $this->removeExtraDataFromMapping($mapping);
      }

      // Extract values.
      $value = $this->extractFieldPropertyValuesFromRawData($field_definition, $property_name, $raw_data, $context);

      $processor_id = $this->getFieldPropertyProcessor($field_definition->getName(), $property_name);
      if (!empty($processor_id)) {
        $processor = $this->fieldMapProcessorManager->createInstance($processor_id);
        $processor->setExtraData($extra_data);
        $value = $processor->processData($value, $field_definition);
      }

      // Make sure to not set references to empty arrays.
      if ($property_name === 'target_id' && (empty($value) || empty($value[0]))) {
        continue;
      }
      $properties_values[$property_name] = $value;
    }

    // We have now collected all the mappable property values. Let's merge them
    // together so they represent a single field values array.
    $field_values = [];
    foreach ($properties_values as $property_name => $property_values) {
      if (is_array($property_values)) {
        foreach ($property_values as $delta => $property_value) {
          $field_values[$delta][$property_name] = $property_value;
        }
      }
      else {
        $field_values[0][$property_name] = $property_values;
      }
    }

    // Now that we have the field values, and the amount of deltas is known, we
    // can add in the constant mapped values.
    foreach ($property_mappings as $property_name => $mapping) {
      if (!$this->isConstantValueMapping($mapping)) {
        continue;
      }

      if (!empty($field_values)) {
        foreach ($field_values as &$field_value) {
          $field_value[$property_name] = $this->getMappedConstantValue($mapping);
        }
      }
      else {
        $field_values = [
          [$property_name => $this->getMappedConstantValue($mapping)],
        ];
      }
    }
    return $field_values;
  }

  /**
   * {@inheritdoc}
   */
  protected function extractFieldPropertyValuesFromRawData(FieldDefinitionInterface $field_definition,
                                                                                    $property_name,
                                                           array                    $raw_data,
                                                           array                    &$context): array {
    $mapping = $this->getFieldPropertyMapping($field_definition->getName(), $property_name);

    if (!$mapping) {
      return [];
    }
    $mapping = $this->removeExtraDataFromMapping($mapping);
    $mapping_keys = explode('/', $mapping);

    return $this->recursiveMapFieldFromRawData(
      $raw_data,
      $mapping_keys,
      [],
      $field_definition,
      0,
      $property_name
    );
  }

  /**
   * Extracts extra mapping data.
   *
   * @param string $mapping
   *   The mapping value to extract.
   *
   * @return array
   *   An array of values, or NULL if none.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getExtraMappingData($mapping) {
    $values = explode($this->getExtraMappingValuesSeparator(), $mapping);
    array_shift($values);
    $extra_data = [];
    foreach ($values as $value) {
      [$key, $value] = explode('=', $value);
      $extra_data[$key] = $value;
    }
    return $extra_data;
  }

  /**
   * Extract mapping field removing extra data..
   *
   * @param string $mapping
   *   The mapping value to check.
   *
   * @return string
   *   The mapping value without extra data.
   */
  protected function removeExtraDataFromMapping(mixed $mapping) {
    $values = explode($this->getExtraMappingValuesSeparator(), $mapping);
    return reset($values);
  }

  /**
   * Check if the mapping provides a constant value.
   *
   * @param string $mapping
   *   The mapping value to check.
   *
   * @return bool
   *   TRUE if the mapping provides a constant value, FALSE otherwise.
   */
  protected function haveExtraMappingData($mapping) {
    return $this->getExtraMappingValuesSeparator()
      && is_string($mapping)
      && strpos($mapping, $this->getExtraMappingValuesSeparator()) > 0;
  }

  /**
   * Get the extra mapping values separator..
   *
   * @return string
   *   The constant value.
   */
  protected function getExtraMappingValuesSeparator() {
    return '|';
  }

}
