<?php

namespace Drupal\external_entities_field_processors\Plugin\ExternalEntities\FieldMapProcessor;

use Drupal\external_entities_field_processors\FieldMapProcessor\FieldMapProcessorBase;

/**
 * Date String Processor.
 *
 * @FieldMapProcessor(
 *   id = "date_string_to_date_field",
 *   label = @Translation("Date string to date field"),
 *   description = @Translation("Converts date string to Drupal date field compatible data (Y-m-d)."),
 *   field_types = ""
 * )
 *
 * @package Drupal\external_entities_field_processors\Plugin\ExternalEntities\FieldMapProcessor
 */
class DateStringToDatefield extends FieldMapProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function processData($raw_data): array|string {
    $source_date = is_array($raw_data) ? implode(' ', $raw_data) : $raw_data;
    $timestamp = strtotime($source_date);
    return $timestamp === FALSE ? [] : [date('Y-m-d', $timestamp)];
  }

}
