<?php

namespace Drupal\external_entities_field_processors\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a field map processor annotation object.
 *
 * @see \Drupal\external_entities\StorageClient\FieldMapperManager
 * @see plugin_api
 *
 * @Annotation
 */
class FieldMapProcessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-friendly name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The field types this formatter applies to separated by comma (optional).
   *
   * @var string|null
   */
  public $field_types;

}
